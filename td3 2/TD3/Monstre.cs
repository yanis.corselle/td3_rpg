﻿using System;
namespace TD3
{
    public abstract class Monstre
    {
        double valeurAttaque;
        double pointsVie;

        public double PointsVie { get => pointsVie; set => pointsVie = value; }
        public double ValeurAttaque { get => valeurAttaque; set => valeurAttaque = value; }
    }
}
