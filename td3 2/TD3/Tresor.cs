﻿namespace TD3
{
     public class Tresor
    {
        int potionOuPiege;
        int nbEffet;

        public Tresor(int potionOuPiege, int nbEffet)
        {
            this.potionOuPiege = potionOuPiege;
            this.nbEffet = nbEffet;
        }

        public int NbEffet { get => nbEffet; set => nbEffet = value; }
        public int PotionOuPiege { get => potionOuPiege; set => potionOuPiege = value; }
    }
}