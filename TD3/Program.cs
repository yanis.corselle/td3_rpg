﻿using System;

namespace TD3
{
    public class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            Heros heros = new Heros(rnd.Next(50, 100), rnd.Next(50, 100));
            Console.WriteLine("Début du jeu ! Le hèros possède " + heros.PointsVie + " points de vie et " + heros.ValeurAttaque + " points d'attaque.");
            int nbCombat = 0;
            while (heros.PointsVie > 0)
            {
                nbCombat++;
                Monstre monstre = GestionnaireDePartie.AppelMonstre(rnd.Next(20, 80), rnd.Next(20, 80));
                Console.WriteLine("Début du combat n°" + nbCombat + "! Le " + monstre.GetType()+ "  possède " + monstre.PointsVie + " points de vie et " + monstre.ValeurAttaque + " points d'attaque.");
                Combat(heros, monstre);
                if (heros.PointsVie > 0)
                {
                    if(nbCombat == 5)
                    {
                        heros = GestionnaireDePartie.EvolutionHeros(heros);
                    }
                    
                    Console.WriteLine("le héros a gagné le combat n°" + nbCombat + ".");
                    Tresor tresor = new Tresor(rnd.Next(2), rnd.Next(20));
                    heros = TrouverTresor(heros, tresor);
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("le héros a perdu le combat n°" + nbCombat);
                }
                
                
            }

        }


        public static Heros TrouverTresor(Heros heros, Tresor tresor)
        {
            if (tresor.PotionOuPiege == 1)
            {
                heros.PointsVie -= tresor.NbEffet;
                Console.WriteLine("Le héros a trouvé un piège et a perdu " + tresor.NbEffet + " points de vie.");
            }
            else
            {
                heros.PointsVie += tresor.NbEffet;
                Console.WriteLine("Le héros a trouvé une potion et a récupéré " + tresor.NbEffet + " points de vie.");
            }

            return heros;
        }

        public static Heros Combat(Heros heros, Monstre monstre)
        {
            Random rnd = new Random();
            int nbTour = 0;
            double pointsVie_departCombat = heros.PointsVie;
            while (monstre.PointsVie > 0 && heros.PointsVie > 0)
            {
                nbTour++;
                Console.WriteLine("Début du tour n°" + nbTour + "! Il reste " + heros.PointsVie + " points de vie au héros et " + monstre.PointsVie + " au " + monstre.GetType());
                monstre.PointsVie -= heros.ValeurAttaque;
                if (monstre.PointsVie > 0)
                {
                    heros.PointsVie -= monstre.ValeurAttaque;
                }

            }

            return heros;
        }

    }
}
