﻿using System;
namespace TD3
{
    public class Sorciere : Monstre
    {
        public Sorciere(double valeurAttaque, double pointsVie)
        {
            this.ValeurAttaque = Math.Round(valeurAttaque *1.2);
            this.PointsVie = Math.Round(pointsVie *0.8);
        }
    }
}
