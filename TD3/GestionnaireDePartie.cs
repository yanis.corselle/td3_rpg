﻿using System;
namespace TD3
{
    public class GestionnaireDePartie
    {

        public static Monstre AppelMonstre(double valeurAttaque, double pointsVie)
        {
            Random rand = new Random();
            int typeMonstre = rand.Next(4);
            Monstre monstre;
            if(typeMonstre == 1)
            {
                monstre = new Gobelin(valeurAttaque, pointsVie);
            }

            if (typeMonstre == 2)
            {
                monstre = new Sorciere(valeurAttaque, pointsVie);
            }

            else
            {
                monstre = new Squelette(valeurAttaque, pointsVie);
            }

            return monstre;
        }

        public static Heros EvolutionHeros(Heros heros)
        {
            Console.WriteLine();
            Console.WriteLine("Votre héros est en train dévoluer !");
            Random rand = new Random();
            int typeHeros = rand.Next(4);
            if (typeHeros == 1)
            {
                heros.PointsVie = heros.PointsVie * 1.2;
                heros.ValeurAttaque = Math.Round(heros.ValeurAttaque * 1.1);
                Console.WriteLine("Votre héros est devenu un chevalier !");
            }

            if (typeHeros == 2)
            {
                heros.PointsVie = heros.PointsVie * 1.15;
                heros.ValeurAttaque = heros.ValeurAttaque * 1.15;
                Console.WriteLine("Votre héros est devenu un archer !");
            }

            else
            {
                heros.PointsVie = heros.PointsVie * 1.1;
                heros.ValeurAttaque = heros.ValeurAttaque * 1.2;
                Console.WriteLine("Votre héros est devenu un magicien !");
            }

            Math.Round(heros.ValeurAttaque);
            Math.Round(heros.PointsVie);

            return heros;
        }
    }
}
