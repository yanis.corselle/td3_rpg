﻿using System;
namespace TD3
{
    public class Squelette : Monstre
    {
        public Squelette(double valeurAttaque, double pointsVie)
        {
            this.ValeurAttaque = Math.Round(valeurAttaque * 0.8);
            this.PointsVie = Math.Round(pointsVie * 1.2);
        }
    }
}

