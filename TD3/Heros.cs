﻿using System;
namespace TD3
{
    public class Heros
    {
        double valeurAttaque;
        double pointsVie;

        public Heros(double valeurAttaque, double pointsVie)
        {
            this.valeurAttaque = valeurAttaque;
            this.pointsVie = pointsVie;
        }

        public double PointsVie { get => pointsVie; set => pointsVie = value; }
        public double ValeurAttaque { get => valeurAttaque; set => valeurAttaque = value; }
    }
}
