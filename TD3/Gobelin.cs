﻿using System;
namespace TD3
{
    public class Gobelin : Monstre
    {
        public Gobelin(double valeurAttaque, double pointsVie)
        {
            this.ValeurAttaque = Math.Round(valeurAttaque *1.1);
            this.PointsVie = Math.Round(pointsVie * 0.9);
        }
    }
}
